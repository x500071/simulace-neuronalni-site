from math import pow
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
import random
import networkx as nx
from scipy.stats import truncnorm

######################################################################################################
# SITE PRO SPRAZENI


def no_coupling(n):
    K = []
    for row in range(n):
        line = []
        for col in range(n):
            line.append(0)
        K.append(line)
    return np.asarray(K)


def small_world(n, k, p, seed):
    # n..pocet neuronu, k..s kolika neurony je propojen, p..pst prepojeni
    G = nx.watts_strogatz_graph(n, k, p, seed)
    return nx.adjacency_matrix(G).todense()


def all_to_all(n):
    K = []
    for row in range(n):
        line = []
        for col in range(n):
            if row == col:
                line.append(0)
            else:
                line.append(1)
        K.append(line)
    return np.asarray(K)


def random_network(n, p, seed):
    G = nx.erdos_renyi_graph(n, p, seed, directed=False)  # random network
    return nx.adjacency_matrix(G).todense()


def ring(n):
    if n == 1:
        return np.asarray([[0]])
    K = []
    for row in range(n):
        line = []
        for col in range(n):
            if col == (row-1) % n or col == (row+1) % n:
                line.append(1)
            else:
                line.append(0)
        K.append(line)
    return np.asarray(K)

######################################################################################################
# MODEL NEURONU

def interneuron(t, variables, params, stimulus):
    # variables = [Vi, hi, ni]
    # params = [C_par, Iext, gNa, gK, VNa, VK]
    # stimulus = [st_t0, st_tn, st_A, st_r]

    Vi = variables[0]
    hi = variables[1]
    ni = variables[2]

    # parameters
    C_par = params[0]
    C = truncnorm.rvs(C_par[0], C_par[1], loc=C_par[2], scale=C_par[3])
    Iext = params[1]
    A = stimulus[2]
    r = stimulus[3]
    if stimulus[1] <= t <= stimulus[1] + stimulus[0]:
        Iext += A * np.exp(-r * (t - stimulus[1]))
    gNa = params[2]
    gK = params[3]
    VNa = params[4]
    VK = params[5]
    gL = 0.1
    VL = -60

    # function
    mi = 1/(1 + np.exp(-0.08*(Vi+26)))

    hinf = 1 / (1 + np.exp(0.13*(Vi+38)))
    ninf = 1 / (1 + np.exp(-0.045*(Vi+10)))

    tauh = 0.6 / (1 + np.exp(-0.12*(Vi+67)))
    taun = 0.5 + 2 / (1 + np.exp(0.045*(Vi-50)))

    INa = gNa * pow(mi, 3) * hi * (Vi - VNa)
    IK = gK * pow(ni, 4) * (Vi - VK)
    IL = gL * (Vi - VL)

    dVi = 1/C * (Iext - IL - INa - IK)
    dhi = (hinf - hi) / tauh
    dni = (ninf - ni) / taun

    return [dVi, dhi, dni, C]


def coupled_interneuron(t, variables, K, n, params, stimulus):
    # prvne vsechny V, pak vsechny N

    Vi = variables[:n]
    hi = variables[n:2*n]
    ni = variables[2*n:]

    dV = []
    dh = []
    dn = []

    for neuron in range(n):
        dVar = interneuron(t, [variables[neuron], variables[neuron+n], variables[neuron+2*n]], params, stimulus)
        dV.append(dVar[0])
        dh.append(dVar[1])
        dn.append(dVar[2])
        C = dVar[3]

    for row in range(n):
        for col in range(n):
            dV[row] += (Vi[col] - Vi[row]) * K[row][col] / C

    dV.extend(dh)
    dV.extend(dn)
    return dV

######################################################################################################
# EULER-MARUYAMA METODA

def euler_maruyama(sigma_noise, X0, T, dt, N_eq, K, params, stimulus):
    # dX =  f(X) dt + sigma dW
    # f...drift function (fce interneuron s parametry)
    # sigma_noise = sigma_noice / C
    # X0... pocatecni podminky
    # T... delka casoveho intervalu
    # dt... krok
    # N_eq... pocet neuronu

    N = np.floor(T/dt).astype(int) # pocet kroku
    d = len(X0) # pocet rovnic v soustave celkem
    sigma_noise = [sigma_noise]*N_eq + [0]*(d-N_eq) # sigma_noise + (d-N_eq) nul
    X = np.zeros((d, N + 1)) # pocet radku: pocet rovnic interneuronu, pocet sloupcu: pocet kroku+1
    X[:, 0] = X0 # prvni sloupec jsou pocatecni podminky
    t = np.arange(0, T+dt, dt)  #cas
    dW = np.vstack([np.sqrt(dt) * np.random.randn(N_eq, N), np.zeros((d - N_eq, N))]) # prvni radek prirustky Wienerova procesu, pak nuly

    for step in range(N): #pres vsechny kroky
        neuron = coupled_interneuron(t[step], X[:, step], K, N_eq, params, stimulus)
        for i in range(len(neuron)):
            neuron[i] = neuron[i] * dt
        X[:, step + 1] += X[:, step] + neuron + sigma_noise * dW[:, step]
    return X, t


######################################################################################################
if __name__ == '__main__':
    # POCET NEURONU A TYP COUPLINGU
    n = 1  # počet neuronů
    epsilon = 0.02
    p = 0.5
    k = 3
    seed = 123

    K = all_to_all(n)

    K = K * epsilon

    # VOLBA POCATECNICH PODMINEK
    t0 = 30  # pocatek intervalu
    tn = 90  # konec intervalu
    dt = 0.01  # krok

    V0_beg = 30
    V0_end = 30
    V0 = []
    h0 = []
    n0 = []
    for i in range(n):
        V0.append(random.uniform(V0_beg, V0_end))
        h0.append(0.25)
        n0.append(0.5)
    y0 = V0 + h0 + n0

    # STIMULUS
    Iext = -2
    st_t0 = 50
    st_len = 20

    stimulus_0 = [st_len, st_t0, 0, 0]
    stimulus_const = [st_len, st_t0, 40, 0]
    stimulus_exp = [st_len, st_t0, 40, 0.1]

    # VOLBA PARAMETRU
    C_mu = 1
    C_sigma = 0.03
    C_a = 0.91
    C_b = 1.09
    C_par = [C_a, C_b, C_mu, C_sigma] # mu, sigma, a, b
    gNa = 30
    gK = 20
    VNa = 45
    VK = -80
    params = [C_par, Iext, gNa, gK, VNa, VK]

    ######################################################################################################
    # RESENI

    # using Runge Kutta 45
    res_0 = integrate.solve_ivp(coupled_interneuron, [t0, tn], y0, method='RK45', args=[K, n, params, stimulus_0])
    res_const = integrate.solve_ivp(coupled_interneuron, [t0, tn], y0, method='RK45', args=[K, n, params, stimulus_const])
    res_exp = integrate.solve_ivp(coupled_interneuron, [t0, tn], y0, method='RK45', args=[K, n, params, stimulus_exp])

    ######################################################################################################
    # VISUALIZATIONS

    plt.rcParams.update({'font.size': 18})

    # SINGLE NEURONS
    plt.subplot(3, 1, 1)
    V = res_0.y
    T = res_0.t
    for neuron in range(n):
        plt.plot(T, V[neuron], 'blue')
    plt.text(75, -30, 'bez stimulu')
    #plt.ylim(-70, 70)
    #plt.xlabel('t')
    plt.ylabel('V')

    plt.subplot(3, 1, 2)
    V = res_const.y
    T = res_const.t
    for neuron in range(n):
        plt.plot(T, V[neuron], 'blue')
    plt.vlines(x=[st_t0, st_t0+st_len], ymin=-70, ymax=70, linestyles='dotted', colors='darkgrey')
    plt.text(75, 0, 'konstantní stimulus\nIstim = 40')
    #plt.ylim(-40, 30)
    #plt.xlabel('t')
    plt.ylabel('V')

    plt.subplot(3, 1, 3)
    V = res_exp.y
    T = res_exp.t
    for neuron in range(n):
        plt.plot(T, V[neuron], 'blue')
    plt.vlines(x=[st_t0, st_t0+st_len], ymin=-70, ymax=70, linestyles='dotted', colors='darkgrey')
    plt.text(75, 0, 'exponenciální stimulus\nIstim = 40*exp(-0.1(t-t0)')
    #plt.ylim(-40, 30)
    plt.xlabel('t')
    plt.ylabel('V')

    plt.suptitle('Interneuron, různé hodnoty stimulu')
    plt.show()