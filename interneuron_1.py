import random
import scipy.integrate as integrate
import numpy as np
import matplotlib.pyplot as plt
from math import pow
from scipy import signal

# dodelavam Euler-Maruyama

def interneuron(t, variables):
    V = variables[0]
    hi = variables[1]
    ni = variables[2]
    # parameters
    C = 1 # pak se berou random z TN
    #Iext = 20
    Iext = np.random.normal(24, 1)
    gL = 0.1
    gNa = 30
    gK = 20
    VL = -60
    VNa = 45
    VK = -80

    # function
    mi = 1/(1 + np.exp(-0.08*(V+26)))

    hinf = 1 / (1 + np.exp(0.13*(V+38)))
    ninf = 1 / (1 + np.exp(-0.045*(V+10)))

    tauh = 0.6 / (1 + np.exp(-0.12*(V+67)))
    taun = 0.5 + 2 / (1 + np.exp(0.045*(V-50)))

    INa = gNa * pow(mi, 3) * hi * (V - VNa)
    IK = gK * pow(ni, 4) * (V - VK)
    IL = gL * (V - VL)

    dV = 1/C * (Iext - IL - INa - IK)
    dhi = (hinf - hi) / tauh
    dni = (ninf - ni) / taun

    #print(dV, dhi, dni)
    return [dV, dhi, dni]

def euler_maruyama(sigma_noise, X0, T, dt, N_eq):
    # dX =  f(X) dt + sigma dW
    # f...drift function (fce interneuron s parametry)
    # sigma_noise = sigma_noice / C
    # X0... pocatecni podminky
    # T... delka casoveho intervalu
    # dt... krok
    # N_eq... pocet neuronu

    N = np.floor(T/dt).astype(int) # pocet kroku
    d = len(X0) # pocet rovnic v soustave pro jeden neuron
    sigma_noise = [sigma_noise, 0, 0] # sigma_noise + (d-N_eq) nul
    X = np.zeros((d, N + 1)) # pocet radku: pocet rovnic interneuronu, pocet sloupcu: pocet kroku+1
    X[:, 0] = X0 # prvni sloupec jsou pocatecni podminky
    t = np.arange(0, T+dt, dt)  #cas
    dW = np.vstack([np.sqrt(dt) * np.random.randn(N_eq, N), np.zeros((d - N_eq, N))]) # prvni radek prirustky Wienerova procesu, pak nuly

    for step in range(N): #pres vsechny kroky
        neuron = interneuron(t, X[:, step])
        for i in range(N_eq*3):
            neuron[i] = neuron[i] * dt
        X[:, step + 1] += X[:, step] + neuron + sigma_noise * dW[:, step]
        #X[:, neuron+1] += interneuron(t, X[:, neuron]) * dt + sigma_noise * dW[:, neuron]
    print("EM hotovka")
    return X, t



if __name__ == '__main__':
    t0 = 0
    tbound = 50
    maxstep = 10000
    dt = 0.01

    #V1, h1, n1 = simulate(0, 0.3, 0.42, t)
    #V2, h2, n2 = simulate(-10, 0.05, 0.55, t)
    y0 = [40, 0.25, 0.5]
    C = 1

    sigma_noise = 1 # std of white noise added to the signal during integration

    if sigma_noise == 0:
        res = integrate.solve_ivp(interneuron, [t0, tbound], y0, method='RK45')
        V = res.y
        T = res.t
    else:
        print("Use Euler-Maruyama solver!")
        V, T = euler_maruyama(sigma_noise/C, y0, tbound, dt, 1)
        print(T[0], T[-1])

    plt.plot(T, V[0])
    plt.show()

    nfft = 2 ** 18
    fs = 1000 / dt

    #f, Pxx = signal.periodogram(V[0], fs, nfft=nfft)
    f, Pxx = signal.periodogram(signal.detrend(V[0]), fs, nfft=nfft)
    plt.semilogy(f, Pxx)
    #plt.ylim([1e-7, 1e2])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()

