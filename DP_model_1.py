import scipy.integrate as integrate
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal


def dp(p, alpha, beta):
    return alpha*(1-p) - beta*p


def destexhe_pare(t, variables):
    # [V, m,h,n,mM]
    V = variables[0]
    m = variables[1]
    h = variables[2]
    n = variables[3]
    mM = variables[4]

    # parameters
    C = 1
    Iext = 10
    g_L = 0.019
    g_Na = 120
    g_Kdr = 100
    g_M = 2
    V_L = -65
    V_Na = 55
    V_K = -85

    V_T = -58
    V_S = -10

    # other variables
    alpha_m = (-0.32 * (V - V_T - 13)) / (np.exp(-(V - V_T - 13) / 4) - 1)
    beta_m = (0.28 * (V - V_T - 40)) / (np.exp((V - V_T - 40) / 5) - 1)
    dm = dp(m, alpha_m, beta_m)

    alpha_h = 0.128 * np.exp(-(V - V_T - V_S - 17) / 18)
    beta_h = 4 / (1 + np.exp(-(V - V_T - V_S - 40) / 5))
    dh = dp(h, alpha_h, beta_h)

    alpha_n = (-0.032 * (V - V_T - 15)) / (np.exp(-(V - V_T - 15) / 5) - 1)
    beta_n = 0.5 * np.exp(-(V-V_T-10)/40)
    dn = dp(n, alpha_n, beta_n)

    alpha_mM = (0.0001 * (V+30))/(1-np.exp(-(V+30)/9))
    beta_mM = (-0.0001 * (V+30))/(1-np.exp(V+30)/9)
    dmM = dp(mM, alpha_mM, beta_mM)

    # function
    I_L = g_L * (V - V_L)
    I_Na = g_Na * m**3 * h * (V - V_Na)
    I_Kdr = g_Kdr * n**4 * (V - V_K)
    I_M = g_M * mM * (V - V_K)

    dV = 1/C * (Iext - I_L - I_Na - I_Kdr - I_M)

    return [dV, dm, dh, dn, dmM]


if __name__ == '__main__':
    t0 = 0
    tbound = 50
    maxstep = 10000

    # [dV, dm, dh, dn, dmM]
    y0 = [0, 0.3, 0.42, 0.3, 0.4]

    res = integrate.solve_ivp(destexhe_pare, [t0, tbound], y0, method='RK45')
    V = res.y
    T = res.t

    plt.plot(T, V[0])
    plt.show()

    # PERIODOGRAM
    dt = 0.0001
    fs = 1000 / dt

    f, Pxx_den = signal.periodogram(V[0], fs)
    plt.semilogy(f, Pxx_den)
    plt.ylim([1e-7, 1e2])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()
